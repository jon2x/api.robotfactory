# frozen_string_literal: true
500.times do |_n|
  FactoryGirl.create_list(:robot, Faker::Number.between(1, 4), :will_pass_the_qa, name: Faker::FunnyName.name)
  robot = Robot.create!(name: Faker::FunnyName.name)
  RobotConfig.create!(
    robot: robot,
    has_sentience: Faker::Boolean.boolean,
    has_wheels: Faker::Boolean.boolean,
    has_tracks: Faker::Boolean.boolean,
    number_of_rotors: Faker::Number.between(1, 10),
    colour: Faker::Color.color_name
  )
  statuses = [
    RobotStatus::ON_FIRE,
    RobotStatus::RUSTY,
    RobotStatus::LOOSE_SCREWS,
    RobotStatus::PAINT_SCRATCHED
  ].freeze

  number_of_statuses = 4.times.to_a.sample

  number_of_statuses.times.each do |_m|
    status = statuses.sample
    robot.robot_statuses.create!(status: status) unless robot.has_status_of(status)
  end

  puts "Robot #{robot.name} has been created"
end