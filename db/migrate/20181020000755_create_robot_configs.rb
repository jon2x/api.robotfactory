class CreateRobotConfigs < ActiveRecord::Migration[5.1]
  def up
    create_table :robot_configs do |t|
      t.boolean :has_sentience, default: false
      t.boolean :has_wheels, default: false
      t.boolean :has_tracks, default: false
      t.integer :number_of_rotors, default: 0
      t.string :colour
      t.references :robot, index: true, foreign_key: true
      t.timestamps
    end
  end

  def down
    drop_table :robot_configs
  end
end

# hasSentience: boolean,
# hasWheels: boolean,
# hasTracks: boolean,
# numberOfRotors: integer,
# Colour: string