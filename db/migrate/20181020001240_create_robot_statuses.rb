class CreateRobotStatuses < ActiveRecord::Migration[5.1]
  def up
    create_table :robot_statuses do |t|
      t.string :status, default: 'on fire'
      t.references :robot, index: true, foreign_key: true
      t.timestamps
    end
  end

  def down
    drop_table :robot_statuses
  end
end
