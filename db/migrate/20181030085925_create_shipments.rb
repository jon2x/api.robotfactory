class CreateShipments < ActiveRecord::Migration[5.1]
  def up
    create_table :shipments do |t|
      t.string :name
      t.timestamps
    end

    add_reference :robots, :shipment, index: true, foreign_key: true
  end

  def down
    drop_table :shipments
    remove_reference :robots, :shipment
  end
end
