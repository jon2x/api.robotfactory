class RobotStatus < ApplicationRecord
  belongs_to :robot

  # statuses
  ON_FIRE = 'on fire'.freeze
  RUSTY = 'rusty'.freeze
  LOOSE_SCREWS = 'loose screws'.freeze
  PAINT_SCRATCHED = 'paint scratched'.freeze
  FACTORY_SECOND = 'factory seconds'.freeze
  PASSED_QA = 'passed qa'.freeze
  SHIPPED = 'shipped'.freeze
  RECYCLED = 'recycled'.freeze

  # normal number of rotors
  NORMAL_NUMBER_OF_ROTORS = '8'.freeze
end
