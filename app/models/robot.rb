class Robot < ApplicationRecord
  has_many :robot_statuses, dependent: :destroy
  has_one :robot_config, dependent: :destroy
  belongs_to :shipment, required: false

  def has_status_of(status)
    robot_statuses.where(status: status).present?
  end

  def on_fire_statuses
    @on_fire_status ||= robot_statuses.
    where('robot_statuses.status = ?', RobotStatus::ON_FIRE)
  end

  def is_on_fire?
    on_fire_statuses.any? && robot_config.has_sentience
  end

  def qa_passed_statuses
    @qa_passed_statuses ||= robot_statuses.
    where('robot_statuses.status = ?', RobotStatus::PASSED_QA)
  end

  def have_passed_from_qa?
    qa_passed_statuses.any?
  end

  def recycled_statuses
    @recycled_statuses ||= robot_statuses.
    where('robot_statuses.status = ?', RobotStatus::RECYCLED)
  end

  def has_been_sent_to_second_factory?
    second_factory_statuses.any?
  end

  def second_factory_statuses
    @recycled_statuses ||= robot_statuses.
    where('robot_statuses.status = ?', RobotStatus::FACTORY_SECOND)
  end

  def has_been_recycled?
    recycled_statuses.any?
  end

  def shipped_statuses
    @recycled_statuses ||= robot_statuses.
    where('robot_statuses.status = ?', RobotStatus::SHIPPED)
  end

  def has_been_shipped?
    shipped_statuses.any?
  end

  def extinguish
    on_fire_statuses.destroy_all if is_on_fire?
  end

  def set_shipped_status
    robot_statuses.destroy_all
    robot_statuses.create(status: RobotStatus::SHIPPED)
  end

  def set_to_be_recycled
    robot_statuses.destroy_all
    robot_statuses.create(status: RobotStatus::RECYCLED)
  end

  def set_to_pass_the_qa
    robot_statuses.destroy_all
    robot_statuses.create!(status: RobotStatus::PASSED_QA)
  end
  
  def set_to_be_sent_to_second_factory
    robot_statuses.destroy_all
    robot_statuses.create!(status: RobotStatus::FACTORY_SECOND)
  end

  class << self
    def unprocessed(args = {})
      page = args[:page] || 1
      per = args[:per] || 20
      robots =  joins(:robot_statuses, :robot_config).
      includes(:robot_statuses, :robot_config).
      where('robot_statuses.status NOT IN (?) OR robot_statuses.status IS NULL', [
        RobotStatus::FACTORY_SECOND,
        RobotStatus::PASSED_QA,
        RobotStatus::SHIPPED,
        RobotStatus::RECYCLED
      ]).page(page).per(per)
      bot_list_resp(robots)
    end

    def qa_passed(args = {})
      page = args[:page] || 1
      per = args[:per] || 20
      robots =  joins(:robot_statuses, :robot_config).
      includes(:robot_statuses, :robot_config).
      where('robot_statuses.status IN (?)', [
        RobotStatus::PASSED_QA
      ]).page(page).per(per)
      bot_list_resp(robots)
    end

    def factory_second(args = {})
      page = args[:page] || 1
      per = args[:per] || 20
      robots =  joins(:robot_statuses, :robot_config).
      includes(:robot_statuses, :robot_config).
      where('robot_statuses.status IN (?)', [
        RobotStatus::FACTORY_SECOND,
      ]).page(page).per(per)
      bot_list_resp(robots)
    end

    def bot_list_resp(bots)
      {
        robots: bots.map(&:serialize),
        meta: {
          pagination: {
            current_page: bots.current_page,
            next_page: bots.next_page,
            prev_page: bots.prev_page,
            total_page: bots.total_pages
          }
        }
      }
    end
  end

  def serialize
    {
      id: id,
      name: name,
      avatar: "https://robohash.org/#{id}",
      statuses: robot_statuses.map(&:status).compact,
      configs: {
        has_sentience: robot_config.has_sentience,
        has_wheels: robot_config.has_wheels,
        has_tracks: robot_config.has_tracks,
        number_of_rotors: robot_config.number_of_rotors,
        colour: robot_config.colour
      },
      production_statuses: {
        passable: passable?,
        recyclable: recyclable?,
        second_factory: second_factory?,
        on_fire: on_fire?
      }
    }
  end

  def recyclable?
    ((robot_config.number_of_rotors < 3 || robot_config.number_of_rotors > 8) ||
    (robot_config.number_of_rotors > 0 && robot_config.colour == 'blue') ||
    (robot_config.has_wheels && robot_config.has_tracks) ||
    (robot_config.has_wheels && rusty?) ||
    (robot_config.has_sentience && loose_screws?) ||
    has_fire?)
  end

  def second_factory?
    (!recyclable? && has_defect?) || have_been_sent_to_second_factory?
  end

  def passable?
    ((!recyclable? && !has_defect?) || no_status? || have_passed_from_qa?) && !have_been_sent_to_second_factory?
  end

  def rusty?
    robot_statuses.map(&:status).include?(RobotStatus::RUSTY)
  end

  def loose_screws?
    robot_statuses.map(&:status).include?(RobotStatus::LOOSE_SCREWS)
  end

  def on_fire?
    has_fire? && robot_config.has_sentience
  end

  def has_fire?
    robot_statuses.map(&:status).include?(RobotStatus::ON_FIRE)
  end
  
  def paint_scratched?
    robot_statuses.map(&:status).include?(RobotStatus::PAINT_SCRATCHED)
  end

  def has_defect?
    rusty? || paint_scratched? || loose_screws?
  end

  def have_been_sent_to_second_factory?
    robot_statuses.map(&:status).include?(RobotStatus::FACTORY_SECOND)
  end

  def have_passed_from_qa?
    robot_statuses.map(&:status).include?(RobotStatus::PASSED_QA)
  end

  def no_status?
    robot_statuses.map(&:status).compact.empty?
  end
end
