class Shipment < ApplicationRecord
  has_many :robots

  class << self
    def lists(args = {})
      page = args[:page] || 1
      per = args[:per] || 20
      shipments = order('created_at DESC').page(page).per(per)
      {
        shipments: shipments.map(&:serialize),
        meta: {
          pagination: {
            current_page: shipments.current_page,
            next_page: shipments.next_page,
            prev_page: shipments.prev_page,
            total_page: shipments.total_pages
          }
        }
      }
    end

    def create_shipment(args = {})
      robots = Robot.where(id: args[:robot_ids])
      shipment = create!(name: "SHIPMENT-#{Time.now.to_i}")
      shipment.robots = robots
      robots.map(&:set_shipped_status)
      shipment.serialize
    end
  end

  def serialize
    {
      id: id,
      name: name,
      bots: robots.map(&:serialize)
    }
  end
end
