class Robot
  module QualityAssurance
    class Actions
      attr_accessor :robots

      def initialize(args = {})
        @robots = Robot.where(id: args[:robot_ids] || [])
      end

      def extinguish
        robots.each(&:extinguish)
        true
      end

      def recycle
        robots.each(&:set_to_be_recycled)
        true
      end

      def set_to_pass
        robots.each(&:set_to_pass_the_qa)
        true
      end

      def send_to_second_factory
        robots.each(&:set_to_be_sent_to_second_factory)
        true
      end

      def ship_it
        raise "You can't ship a robot that does not passed the qa" unless robots.all?(&:have_passed_from_qa?)
        shipment = Shipment.create!(name: "ROBOT-SHIPMENT-##{Time.now.to_i}")
        shipment.robots = robots
        robots.each(&:set_shipped_status)
      end
    end
  end
end