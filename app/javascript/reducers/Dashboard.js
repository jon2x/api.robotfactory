import {
  DASHBOARD_FETCH_UNPROCESSED_BOTS,
  DASHBOARD_SET_UNPROCESSED_BOTS,
  DASHBOARD_SET_UNPROCESSED_SELECTED_BOTS,
  DASHBOARD_FETCH_QA_PASSED_BOTS,
  DASHBOARD_SET_QA_PASSED_BOTS,
  DASHBOARD_SET_QA_PASSED_SELECTED_BOTS,
  DASHBOARD_FETCH_FACTORY_SECOND_FACTORY_BOTS,
  DASHBOARD_SET_FACTORY_SECOND_FACTORY_BOTS,
  DASHBOARD_SET_FACTORY_SECOND_SELECTED_BOTS,
  DASHBOARD_FETCH_SHIPPED_BOTS,
  DASHBOARD_SET_SHIPPED_BOTS,
  DASHBOARD_ACTIVE_TAB,
  DASHBOARD_UPDATE_PROPS
} from '../actions/types'

const initialState = {
  unprocessed: {
    bots: [],
    loading: false,
    meta: {},
    selected: []
  },
  qa_passed: {
    bots: [],
    loading: false,
    meta: {},
    selected: []
  },
  factory_second: {
    bots: [],
    loading: false,
    meta: {},
    selected: []
  },
  shipping: {
    shipments: [],
    loading: false,
    meta: {}
  },
  active_tab: 1,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case DASHBOARD_FETCH_UNPROCESSED_BOTS: {
      return {
        ...state,
        unprocessed: {
          ...state.unprocessed,
          loading: true
        }
      }
    }
    case DASHBOARD_SET_UNPROCESSED_BOTS: {
      return {
        ...state,
        unprocessed: {
          ...state.unprocessed,
          loading: false,
          bots: action.payload.robots,
          meta: action.payload.meta
        }
      }
    }
    case DASHBOARD_SET_UNPROCESSED_SELECTED_BOTS: {
      return {
        ...state,
        unprocessed: {
          ...state.unprocessed,
          selected: action.payload
        }
      }
    }

    case DASHBOARD_FETCH_QA_PASSED_BOTS: {
      return {
        ...state,
        qa_passed: {
          ...state.qa_passed,
          loading: true
        }
      }
    }
    case DASHBOARD_SET_QA_PASSED_BOTS: {
      return {
        ...state,
        qa_passed: {
          ...state.qa_passed,
          loading: false,
          bots: action.payload.robots,
          meta: action.payload.meta
        }
      }
    }
    case DASHBOARD_SET_QA_PASSED_SELECTED_BOTS: {
      return {
        ...state,
        unprocessed: {
          ...state.unprocessed,
          selected: state.payload
        }
      }
    }

    case DASHBOARD_FETCH_FACTORY_SECOND_FACTORY_BOTS: {
      return {
        ...state,
        factory_second: {
          ...state.factory_second,
          loading: true
        }
      }
    }
    case DASHBOARD_SET_FACTORY_SECOND_FACTORY_BOTS: {
      return {
        ...state,
        factory_second: {
          ...state.factory_second,
          loading: false,
          bots: action.payload.robots,
          meta: action.payload.meta
        }
      }
    }
    case DASHBOARD_SET_FACTORY_SECOND_SELECTED_BOTS: {
      return {
        ...state,
        unprocessed: {
          ...state.unprocessed,
          selected: action.payload
        }
      }
    }
    case DASHBOARD_ACTIVE_TAB: {
      return {
        ...state,
        active_tab: action.payload,
      }
    }
    case DASHBOARD_FETCH_SHIPPED_BOTS:
      return {
        ...state,
        shipping: {
          ...state.shipping,
          loading: true
        }
      }
    case DASHBOARD_SET_SHIPPED_BOTS:
      return {
        ...state,
        shipping: {
          ...state.shipping, 
          ...action.payload,
        }
      }
    case DASHBOARD_UPDATE_PROPS:
      return {
        ...state,
        ...action.payload
      }
    default:
      return state
  }
}