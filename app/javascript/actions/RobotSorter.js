export default class RobotSorter {
  constructor(bots = []) {
    this.bots = bots
  }

  passed() {
    return this.sortByProductionStatusType('passable')
  }

  factorySecond() {
    return this.sortByProductionStatusType('second_factory')
  }

  recyclables() {
    return this.sortByProductionStatusType('recyclable')
  }

  onFire = () => {
    return this.sortByProductionStatusType('on_fire')
  }

  sortByProductionStatusType(type = '') {
    const res = []
    this.bots.forEach((bot) => {
      if (bot.production_statuses[type]) {
        res.push(bot)
      }
    })
    return res
  }
}