import {
  DASHBOARD_FETCH_UNPROCESSED_BOTS,
  DASHBOARD_SET_UNPROCESSED_BOTS,
  DASHBOARD_SET_UNPROCESSED_SELECTED_BOTS,
  DASHBOARD_FETCH_QA_PASSED_BOTS,
  DASHBOARD_SET_QA_PASSED_BOTS,
  DASHBOARD_SET_QA_PASSED_SELECTED_BOTS,
  DASHBOARD_FETCH_FACTORY_SECOND_FACTORY_BOTS,
  DASHBOARD_SET_FACTORY_SECOND_FACTORY_BOTS,
  DASHBOARD_SET_FACTORY_SECOND_SELECTED_BOTS,
  DASHBOARD_FETCH_SHIPPED_BOTS,
  DASHBOARD_SET_SHIPPED_BOTS,
  DASHBOARD_ACTIVE_TAB,
  DASHBOARD_UPDATE_PROPS,
} from './types'

export const requestDispatchTypes = {
  unprocessed: {
    load: DASHBOARD_FETCH_UNPROCESSED_BOTS,
    set: DASHBOARD_SET_UNPROCESSED_BOTS,
    setSelectedBots: DASHBOARD_SET_UNPROCESSED_SELECTED_BOTS,
  },
  qa_passed: {
    load: DASHBOARD_FETCH_QA_PASSED_BOTS,
    set: DASHBOARD_SET_QA_PASSED_BOTS,
    setSelected: DASHBOARD_SET_QA_PASSED_SELECTED_BOTS,
  },
  factory_second: {
    load: DASHBOARD_FETCH_FACTORY_SECOND_FACTORY_BOTS,
    set: DASHBOARD_SET_FACTORY_SECOND_FACTORY_BOTS,
    setSelectedBots: DASHBOARD_SET_FACTORY_SECOND_SELECTED_BOTS,
  },
  shipments: {
    load: DASHBOARD_FETCH_SHIPPED_BOTS,
    set: DASHBOARD_SET_SHIPPED_BOTS
  }
} 

export const fetchRobots = (args = {}) => dispatch => {
  const dispatchType = args.dispatchType
  const requestDispatchType = requestDispatchTypes[dispatchType]
  if (!requestDispatchType) { return null }
  dispatch({
    type: requestDispatchType.load
  })
  const params = buildParams(args)
  dispatch(
    requests(
      `robots/${dispatchType}.json?${params}`,
      'GET',
      {},
      (res) => {
        dispatch({
          type: requestDispatchType.set,
          payload: res
        })
      }
    )
  )
}

export const qaPassRobots = (args) => dispatch => {
  dispatch(
    requests(
      `/robots/qa_pass.json`,
      'POST',
      {robot_ids: args.robot_ids},
      _res => {
        dispatch(fetchRobots({
          dispatchType: 'qa_passed',
          page: args.page || 1,
          per: args.per || 20
        }))
        dispatch(fetchRobots({
          dispatchType: 'unprocessed',
          page: args.page || 1,
          per: args.per || 20
        }))
        dispatch(setActiveTab(2))
      }
    )
  )
}

export const sendRobotToSecondFactory = (args) => dispatch => {
  dispatch(
    requests(
      `/robots/send_to_second_factory.json`,
      'POST',
      {robot_ids: args.robot_ids},
      _res => {
        dispatch(fetchRobots({
          dispatchType: 'qa_passed',
          page: args.page || 1,
          per: args.per || 20
        }))
        dispatch(fetchRobots({
          dispatchType: 'factory_second',
          page: args.page || 1,
          per: args.per || 20
        }))
        dispatch(setActiveTab(3))
      }
    )
  )
}

export const extinguishRobots = (args) => dispatch => {
  dispatch(
    requests(
      `/robots/extinguish.json`,
      'POST',
      {robot_ids: args.robot_ids},
      () => {
        dispatch(fetchRobots({
          dispatchType: 'unprocessed',
          page: args.page || 1,
          per: args.per || 20
        }))
      }
    )
  )
}

export const recycleRobots = (args) => dispatch => {
  dispatch(
    requests(
      `/robots/recycle.json`,
      'POST',
      { robot_ids: args.robot_ids },
      (res) => {
        dispatch(fetchRobots({
          dispatchType: 'unprocessed',
          page: args.page || 1,
          per: args.per || 20
        }))
      }
    )
  )
}

export const fetchShipments = (args = {}) => dispatch => {
  const requestDispatchType = requestDispatchTypes.shipments
  if (!requestDispatchType) { return null }
  dispatch({
    type: requestDispatchType.load
  })
  const params = buildParams(args)
  dispatch(
    requests(
      `shipments.json?${params}`,
      'GET',
      {},
      (res) => {
        dispatch({
          type: requestDispatchType.set,
          payload: res
        })
      }
    )
  )
}

export const shipRobots = (args) => dispatch => {
  dispatch({
    type: DASHBOARD_FETCH_SHIPPED_BOTS
  })
  dispatch(
    requests(
      `/shipments.json`,
      'POST',
      {robot_ids: args.robot_ids},
      () => {
        dispatch(fetchShipments())
        dispatch(setActiveTab(4))
      }
    )
  )
}

const requests = (url, type = 'GET', params = {}, callback = () => {}) => dispatch => {
  fetch(`${url}`, {
    credentials: 'same-origin',
    method: type,
    headers: {
      'content-type': 'application/json'
    },
    body: type === 'GET' ? null : JSON.stringify(params)
  })
  .then(res => res.json())
  .then(res => {
    callback(res)
  })
}

const buildParams = (params) => {
  return Object.entries(params).map(([key, val]) => {
    return `${encodeURIComponent(key)}=${encodeURIComponent(val)}`
  }).join('&')
}

const remove = (array, element) => {
  const index = array.indexOf(element)
  if (index >= 0) { array.splice(index, 1) }
  return array
}

export const toggleSelectedItem = (args = {}) => dispatch => {
  const dispatchType = args.dispatchType
  let action = args.action || 'add'
  let value = args.value
  let selection = args.selection || []
  const requestDispatchType = requestDispatchTypes[dispatchType]
  if (!requestDispatchType) { return null }
  if (action == 'add') {
    selection.push(value)
  } else {
    selection = remove(selection, value)
  }
  dispatch({
    type: requestDispatchType.setSelectedBots,
    payload: selection
  })
}

export const setActiveTab = (active_tab = 1) => dispatch => {
  dispatch({
    type: DASHBOARD_ACTIVE_TAB,
    payload: active_tab
  })
}