import React  from  'react'
import PropTypes  from  'prop-types'
import RobotTable from '../shared/RobotTable'
import Paginator from '../shared/Paginator'
import { Button } from 'react-bootstrap'
import { connect }  from  'react-redux'
import { fetchRobots, shipRobots } from '../../actions/DashboardAction'

class QaPassedRobotTable extends React.Component {
  componentDidMount() {
    this.loadData()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active_tab === 2 && this.props.active_tab !== nextProps.active_tab) {
      this.loadData(this.props.pagination.current_page || 1)
    }
  }

  loadData = (page = 1, per = 20) => {
    this.props.fetchRobots({
      dispatchType: 'qa_passed',
      page: page,
      per: per
    })
  }
  
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-8">
            <div className="p-10">
              <span className="mr-4">Quality Assurance Actions:</span>
              {this.renderShippingButton()}
            </div>
          </div>
          <div className="col-sm-4 text-right">
            <Paginator 
              serviceRequest={this.loadData} 
              pagination={this.props.pagination} />
          </div>
        </div>
        <RobotTable bots={this.props.bots} type={'qa_passed'} />
      </div>
    )
  }

  renderShippingButton() {
    return (
      <Button
        bsSize="xsmall"
        className="mr-4"
        bsStyle="success"
        onClick={this.shipIt}>
        Ship <strong>{this.props.bots.length}</strong> bots
      </Button>
    )
  }

  shipIt = () => {
    this.props.shipRobots({ robot_ids: this.props.bots.map(x => x.id) })
  }
}

QaPassedRobotTable.propTypes = {
  bots: PropTypes.array.isRequired,
  fetchRobots: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  shipRobots: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  bots: state.dashboard.qa_passed.bots,
  loading: state.dashboard.qa_passed.loading,
  pagination: state.dashboard.qa_passed.meta.pagination || {},
  active_tab: state.dashboard.active_tab,
})

export default connect(mapStateToProps, { 
  fetchRobots, 
  shipRobots 
})(QaPassedRobotTable)