import React  from  'react'
import PropTypes  from  'prop-types'
import RobotTable from '../shared/RobotTable'
import Paginator from '../shared/Paginator'
import RobotActions from '../shared/RobotActions'
import { connect }  from  'react-redux'
import { fetchRobots } from '../../actions/DashboardAction'

class UnprocessedRobotTable extends React.Component {
  componentDidMount() {
    this.loadData()
  }
  
  loadData = (page = 1, per = 20) => {
    this.props.fetchRobots({
      dispatchType: 'unprocessed',
      page: page,
      per: per
    })
  }

  render() {
    return (
      <div className="unprocessed-bots">
        <div className="row">
          <div className="col-sm-8">
            <RobotActions type="unprocessed" />
          </div>
          <div className="col-sm-4 text-right">
            <Paginator 
              serviceRequest={this.loadData} 
              pagination={this.props.meta.pagination} />
          </div>
        </div>
        <RobotTable bots={this.props.bots} type={'unprocessed'} />
      </div>
    )
  }
}

UnprocessedRobotTable.propTypes = {
  bots: PropTypes.array.isRequired,
  fetchRobots: PropTypes.func.isRequired,
  meta: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  bots: state.dashboard.unprocessed.bots,
  loading: state.dashboard.unprocessed.loading,
  meta: state.dashboard.unprocessed.meta,
})

export default connect(mapStateToProps, { fetchRobots })(UnprocessedRobotTable)