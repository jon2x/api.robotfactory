import React from 'react'
import { Tabs, Tab } from 'react-bootstrap'
import UnprocessedRobotTable from './UnprocessedRobotTable'
import QaPassedRobotTable from './QaPassedRobotTable'
import FactorySecondRobotTable from './FactorySecondRobotTable'
import RobotShipmentTable from './RobotShipmentTable'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { setActiveTab } from '../../actions/DashboardAction'

class DashboardTabs extends React.Component {
  render () {
    return (
      <Tabs 
        activeKey={this.props.active_tab}
        onSelect={this.handleTabSelect} 
        id="unprocessed-bots-table">
        <Tab eventKey={1} title="Unprocessed">
          <UnprocessedRobotTable />
        </Tab>
        <Tab eventKey={2} title="QA Passed">
          <QaPassedRobotTable />
        </Tab>
        <Tab eventKey={3} title="Secondary Factory">
          <FactorySecondRobotTable />
        </Tab>
        <Tab eventKey={4} title="Shipments">
          <RobotShipmentTable />
        </Tab>
      </Tabs>
    )
  }

  handleTabSelect = (active_tab) => {
    this.props.setActiveTab(active_tab)
  }
}

DashboardTabs.propTypes = {
  active_tab: PropTypes.number.isRequired,
  setActiveTab: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  active_tab: state.dashboard.active_tab
})

export default connect(mapStateToProps, { setActiveTab })(DashboardTabs)
