import React  from  'react'
import PropTypes  from  'prop-types'
import { Table }  from  'react-bootstrap'
import { fetchShipments } from '../../actions/DashboardAction'
import Paginator from '../shared/Paginator'
import ShipmentItemIcon from '../shared/ShipmentItemIcon'
import RobotTableLegends from '../shared/RobotTableLegends'
import { connect } from 'react-redux';

class RobotShipmentTable extends React.Component {
  componentDidMount() {
    this.loadData()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active_tab === 4 && this.props.active_tab !== nextProps.active_tab) {
      this.loadData(this.props.pagination.current_page || 1)
    }
  }

  loadData = (page) => {
    this.props.fetchShipments({ page })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-4 col-sm-offset-8 text-right">
            <Paginator 
              serviceRequest={this.loadData} 
              pagination={this.props.pagination} />
          </div>
        </div>
        <RobotTableLegends />
        <Table responsive>
          <thead>
            {this.renderTableHeadings()}
          </thead>
          <tbody>
            {this.renderEmptyRow()}
            {this.renderTableBody()}
          </tbody>
        </Table>
      </div>
    );
  }

  renderTableHeadings() {
    return (
      <tr>
        <th>Shipment</th>
        <th>Packages</th>
      </tr>
    )
  }

  renderTableBody() {
    return this.props.shipments.map((shipment, key) => {
      return (
        <tr key={`shipment_${key}`}>
          <td>{shipment.name}</td>
          <td>
            {this.renderShipmentItems(shipment.bots)}
          </td>
        </tr>
      )
    })
  }

  renderEmptyRow() {
    if (!this.props.shipments.length) {
      return (
        <tr><td className="text-center" colSpan={2}>No shipment yet here</td></tr>
      )
    }
  }

  renderShipmentItems(bots) {
    return bots.map((bot, key) => {
      return <ShipmentItemIcon key={`botItemIcon_${key}`} bot={bot} />
    })
  }
}

RobotShipmentTable.propTypes = {
  shipments: PropTypes.array.isRequired,
  fetchShipments: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  active_tab: PropTypes.number.isRequired,
  pagination: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  shipments: state.dashboard.shipping.shipments,
  pagination: state.dashboard.shipping.meta.pagination || {},
  loading: state.dashboard.shipping.loading,
  active_tab: state.dashboard.active_tab,
})

export default connect(mapStateToProps, { fetchShipments })(RobotShipmentTable)
