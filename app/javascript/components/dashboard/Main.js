import React from 'react'
import { Tabs, Tab } from 'react-bootstrap'
import { Provider }   from 'react-redux'
import DashboardTabs from './DashboardTabs'

import store from '../../store'

export default class Main extends React.Component {
  render () {
    return (
      <Provider store={store}>
        <div className="container">
          <h1>Dashboard</h1>
          <DashboardTabs />
        </div>
      </Provider>
    )
  }
}