import React  from  'react'
import PropTypes  from  'prop-types'
import RobotTable from '../shared/RobotTable'
import Paginator from '../shared/Paginator'
import { connect }  from  'react-redux'
import { fetchRobots } from '../../actions/DashboardAction'

class FactorySecondRobotTable extends React.Component {
  componentDidMount() {
    this.loadData()
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active_tab === 3 && this.props.active_tab !== nextProps.active_tab) {
      this.loadData(this.props.pagination.current_page || 1)
    }
  }

  loadData = (page = 1, per = 20) => {
    this.props.fetchRobots({
      dispatchType: 'factory_second',
      page: page,
      per: per
    })
  }
  
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-sm-4 col-sm-offset-8 text-right">
            <Paginator 
              serviceRequest={this.loadData} 
              pagination={this.props.pagination} />
          </div>
        </div>
        <RobotTable bots={this.props.bots} type={'factory_second'} />
      </div>
      )
  }
}

FactorySecondRobotTable.propTypes = {
  bots: PropTypes.array.isRequired,
  fetchRobots: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  active_tab: PropTypes.number.isRequired,
}

const mapStateToProps = state => ({
  bots: state.dashboard.factory_second.bots,
  loading: state.dashboard.factory_second.loading,
  pagination: state.dashboard.factory_second.meta.pagination || {},
  active_tab: state.dashboard.active_tab,
})

export default connect(mapStateToProps, { fetchRobots })(FactorySecondRobotTable)