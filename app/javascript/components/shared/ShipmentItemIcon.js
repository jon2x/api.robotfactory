import React from 'react'
import PropTypes from 'prop-types'
import { 
  Table, 
  OverlayTrigger, 
  Popover 
} from 'react-bootstrap'
import RobotStatusIcons from './RobotStatusIcons'
import RobotConfigurations from './RobotConfigurations'

class ShipmentItemIcon extends React.Component {
  render() {
    const pop = (
      <Popover id="-">
        <Table responsive>
          <tbody>
            <tr>
              <td>Name</td>
              <td>{this.props.bot.name}</td>
            </tr>
            <tr>
              <td>Statuses</td>
              <td>{this.renderBotStatuses(this.props.bot.statuses)}</td>
            </tr>
            <tr>
              <td>Configurations</td>
              <td>
                <RobotConfigurations configs={this.props.bot.configs} />
              </td>
            </tr>
          </tbody>
        </Table>
      </Popover>
    )
    return (
      <OverlayTrigger trigger={['hover', 'focus']} placement="right" overlay={pop}>
        <img src={this.props.bot.avatar} width={30} height={30} />
      </OverlayTrigger>
    )
  }

  renderBotStatuses(statuses = []) {
    return statuses.map((status, key) => {
      return (
        <RobotStatusIcons key={`status_${key}`} status={status} />
      )
    })
  }
}

ShipmentItemIcon.propTypes = {
  bot: PropTypes.object.isRequired
}

export default ShipmentItemIcon
