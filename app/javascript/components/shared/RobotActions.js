import React  from  'react'
import PropTypes  from  'prop-types'
import { Button } from 'react-bootstrap'
import { connect }  from  'react-redux'
import { 
  extinguishRobots, 
  recycleRobots,
  qaPassRobots,
  sendRobotToSecondFactory
} from '../../actions/DashboardAction'
import RobotSorter from '../../actions/RobotSorter'

class RobotActions extends React.Component {
  initRobotSorter() {
    this.robotSorter = new RobotSorter(this.props.dashboardData[this.props.type].bots)
  }

  render() {
    return (
      <div className="p-10">
        <span className="mr-4">Quality Assurance Actions:</span>
        {this.renderActionButtons()}
      </div>
    )
  }

  renderActionButtons() {
    this.initRobotSorter()
    return (
      <span>
        {this.renderQaPassButton()}
        {this.renderFactorySecondButton()}
        {this.renderExtinguishButton()}
        {this.renderRecycleButton()}
      </span>
    )
  }

  renderRecycleButton() {
    const selectedBots = this.robotSorter.recyclables()
    if (!selectedBots.length) { return null }
    return (
      <Button
        bsSize="xsmall"
        className="mr-4"
        bsStyle="info"
        onClick={this.recycle}>
        Recycle <strong>{selectedBots.length}</strong> bots
      </Button>
    )
  }

  renderExtinguishButton() {
    const selectedBots = this.robotSorter.onFire()
    if (!selectedBots.length) { return null }
    return (
      <Button
        bsSize="xsmall"
        className="mr-4"
        bsStyle="danger"
        onClick={this.extinguish}>
        Extinguish <strong>{selectedBots.length} ON FIRE</strong> bots
      </Button>
    )
  }

  renderQaPassButton() {
    const selectedBots = this.robotSorter.passed()
    if (!selectedBots.length) { return null }
    return (
      <Button
        bsSize="xsmall"
        className="mr-4"
        bsStyle="success"
        onClick={this.qaPassRobots}>
        Pass <strong>{selectedBots.length} QUALITY</strong> bots
      </Button>
    )
  }

  renderFactorySecondButton() {
    const selectedBots = this.robotSorter.factorySecond()
    if (!selectedBots.length) { return null }
    return (
      <Button
        bsSize="xsmall"
        className="mr-4"
        bsStyle="default"
        onClick={this.sendToSecondFactory}>
        Send <strong>{selectedBots.length}</strong> bots to <strong>Second Factory</strong>
      </Button>
    )
  }

  qaPassRobots = () => {
    this.props.qaPassRobots({ robot_ids: this.robotSorter.passed().map(x => x.id) })
  }

  sendToSecondFactory = () => {
    this.props.sendRobotToSecondFactory({ robot_ids: this.robotSorter.factorySecond().map(x => x.id) })
  }

  recycle = () => {
    this.props.recycleRobots({ robot_ids: this.robotSorter.recyclables().map(x => x.id) })
  }
  
  extinguish = () => {
    this.props.extinguishRobots({ robot_ids: this.robotSorter.onFire().map(x => x.id) })
  }
}

RobotActions.propTypes = {
  extinguishRobots: PropTypes.func.isRequired,
  qaPassRobots: PropTypes.func.isRequired,
  recycleRobots: PropTypes.func.isRequired,
  sendRobotToSecondFactory: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
}

const mapStateToProps = state => ({
  dashboardData: state.dashboard
})

export default connect(mapStateToProps, { 
  extinguishRobots, 
  recycleRobots, 
  qaPassRobots,
  sendRobotToSecondFactory
})(RobotActions)
