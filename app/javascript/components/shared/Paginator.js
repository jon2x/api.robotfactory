import React from 'react';
import PropTypes from 'prop-types'

class Paginator extends React.Component {
  render() {
    if (!this.props.pagination) { return null }
    if (!this.props.pagination.total_page) { return null }
    return (
      <nav>
        <ul className="pagination">
          {this.renderPrev()}
          {this.renderPageInfo()}
          {this.renderNext()}
        </ul>
      </nav>
    )
  }

  renderPrev() {
    if (!this.props.pagination.prev_page) { return null }
    return (
      <li className="page-item">
        <a className="page-link" onClick={this.loadPrevPage}>Prev</a>
      </li>
    )
  }

  renderPageInfo() {
    return (
      <li className="page-item">
        <a className="page-link">{this.props.pagination.current_page} of {this.props.pagination.total_page}</a>
      </li>
    )
  }

  renderNext() {
    if (!this.props.pagination.next_page) { return null }
    return (
      <li className="page-item">
        <a className="page-link" onClick={this.loadNextPage}>Next</a>
      </li>
    )
  }

  loadPrevPage = (_e) => {
    this.props.serviceRequest(this.props.pagination.prev_page)
  }

  loadNextPage = (_e) => {
    this.props.serviceRequest(this.props.pagination.next_page)
  }
}

Paginator.propTypes = {
  pagination: PropTypes.object,
  serviceRequest: PropTypes.func.isRequired
}

export default Paginator