import React  from  'react'
import PropTypes  from  'prop-types'
import RobotStatusIcons from './RobotStatusIcons'
import RobotConfigurations from './RobotConfigurations'
import RobotTableLegends from './RobotTableLegends'
import RobotCheckbox from './RobotCheckbox'
import { Table, Checkbox }  from  'react-bootstrap'
import RobotSorter from '../../actions/RobotSorter'

class RobotTable extends React.Component {  
  render() {
    this.initRobotSorter()
    return (
      <Table responsive>
        <thead>
          <tr>
            <th colSpan={4}><RobotTableLegends /></th>
          </tr>
          {this.renderTableHeadings()}
        </thead>
        <tbody>
          {this.renderEmptyRow()}
          {this.renderTableBody()}
        </tbody>
      </Table>
    );
  }

  initRobotSorter = () => {
    this.robotSorter = new RobotSorter(this.props.bots)
  }

  renderTableHeadings() {
    return (
      <tr>
        <th>
          <Checkbox />
        </th>
        <th>Robot</th>
        <th>Statuses</th>
        <th>Configurations</th>
      </tr>
    )
  }

  generateRobotRowTypeClass = (bot) => {
    if (this.robotSorter.factorySecond().map(x => x.id).includes(bot.id)) {
      return 'default'
    } else if (this.robotSorter.passed().map(x => x.id).includes(bot.id)) {
      return 'success'
    } else if (this.robotSorter.onFire().map(x => x.id).includes(bot.id)) {
      return 'danger'
    } else if (this.robotSorter.recyclables().map(x => x.id).includes(bot.id)) {
      return 'info'
    } else {
      return ''
    }
  }

  renderTableBody() {
    return this.props.bots.map((bot, key) => {
      return (
        <tr className={this.generateRobotRowTypeClass(bot)} key={`bot_${key}`}>
          <td>
            <RobotCheckbox value={bot.id} type={this.props.type} />
          </td>
          <td>
            <img src={bot.avatar} width={50} height={50} /> { bot.name }
          </td>
          <td>
            {this.renderBotStatuses(bot.statuses)}
          </td>
          <td>
            <RobotConfigurations configs={bot.configs} />
          </td>
        </tr>
      )
    })
  }

  renderEmptyRow() {
    if (!this.props.bots.length) {
      return (
        <tr><td className="text-center" colSpan={4}>No bots here</td></tr>
      )
    }
  }

  renderBotStatuses(statuses = []) {
    return statuses.map((status, key) => {
      return (
        <RobotStatusIcons key={`status_${key}`} status={status} />
      )
    })
  }
}

RobotTable.propTypes = {
  bots: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired
}

export default RobotTable
