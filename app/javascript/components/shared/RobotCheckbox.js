import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { toggleSelectedItem } from '../../actions/DashboardAction'
import { Checkbox }  from  'react-bootstrap'

class RobotCheckbox extends React.Component {
  render() {
    const selected = this.selectedBots().includes(this.props.value)
    return (
      <Checkbox 
        checked={selected}
        onChange={this.handleOnChange}
      />
    )
  }

  handleOnChange = (e) => {
    const action = e.target.checked ? 'add' : 'subtract'
    this.props.toggleSelectedItem({
      action: action,
      value: this.props.value,
      selection: this.selectedBots(),
      dispatchType: 'unprocessed'
    })
  }

  selectedBots() {
    return this.props.dashboardData[this.props.type].selected || []
  }
}

RobotCheckbox.propTypes = {
  value: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  dashboardData: PropTypes.object.isRequired,
  toggleSelectedItem: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  dashboardData: state.dashboard
})

export default connect(mapStateToProps, {toggleSelectedItem})(RobotCheckbox)
