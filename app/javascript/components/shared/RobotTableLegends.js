import React  from  'react'
import PropTypes  from  'prop-types'

class RobotTableLegends extends React.Component {
  render() {
    return(this.renderLegends())
  }

  renderLegends() {
    const icons = {
      'on fire': <i className="fa fa-fire mr-2"></i>,
      'rusty': <i className="fa fa-chain-broken mr-2"></i>,
      'loose screws': <i className="fa fa-plus-circle mr-2"></i>,
      'paint scratched': <i className="fa fa-paint-brush mr-2"></i>,
      'factory seconds': <i className="fa fa-industry mr-2"></i>,
      'passed qa': <i className="fa fa-check-circle mr-2"></i>,
      'shipped': <i className="fa fa-truck mr-2"></i>
    }

    return Object.keys(icons).map((label, key) => {
      return (
        <span className="mr-4" key={`legends_${key}`}>
          {icons[label]}{label}
        </span>
      )
    })
  }
}

export default RobotTableLegends