import React from 'react'
import PropTypes  from  'prop-types'
import { OverlayTrigger, Popover } from 'react-bootstrap'

class RobotStatusIcons extends React.Component {
  render() {
    const icons = {
      'on fire': <i className="fa fa-fire mr-2"></i>,
      'rusty': <i className="fa fa-chain-broken mr-2"></i>,
      'loose screws': <i className="fa fa-plus-circle mr-2"></i>,
      'paint scratched': <i className="fa fa-paint-brush mr-2"></i>,
      'factory seconds': <i className="fa fa-industry mr-2"></i>,
      'passed qa': <i className="fa fa-check-circle mr-2"></i>,
      'shipped': <i className="fa fa-truck mr-2"></i>
    }
    const pop = (
      <Popover id="-">
        <span>{icons[this.props.status]}{this.props.status}</span>
      </Popover>
    )
    return (
      <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={pop}>
        <span>{icons[this.props.status]}</span>
      </OverlayTrigger>
    )
  }
}

RobotStatusIcons.propTypes = {
  status: PropTypes.string.isRequired
}

export default RobotStatusIcons;
