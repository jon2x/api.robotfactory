import React from 'react'
import PropTypes  from  'prop-types'
import { ListGroup, ListGroupItem, Label } from 'react-bootstrap'

class RobotConfigurations extends React.Component {
  render() {
    const configs = this.props.configs
    return (
      <ul className="robot-configs">
        <li>
          <span>Sentience:</span>{this.renderBoolean(configs.has_sentience)}
        </li>
        <li>
          <span>Wheels:</span>{this.renderBoolean(configs.has_wheels)}
        </li>
        <li>
          <span>Tracks:</span>{this.renderBoolean(configs.has_tracks)}
        </li>
        <li><span>Rotors:</span>{configs.number_of_rotors}</li>
        <li>
          <span>Colour:</span><Label style={{ background: configs.colour }}>{configs.colour}</Label>
        </li>
      </ul>
    )
  }

  renderBoolean(bool = false) {
    return bool ? <Label bsStyle="success">Yes</Label> : <Label bsStyle="default">No</Label>
  }
}

RobotConfigurations.propTypes = {
  configs: PropTypes.object.isRequired
}

export default RobotConfigurations