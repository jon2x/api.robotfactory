class ShipmentsController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def index
    render json: Shipment.lists(params)
  end

  def create
    render json: Shipment.create_shipment(params)
  end

end
