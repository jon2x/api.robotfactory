class RobotsController < ApplicationController
  attr_accessor :action

  before_action :init_qa_action, only: [:extinguish, :recycle, :qa_pass, :send_to_second_factory]
  skip_before_action :verify_authenticity_token

  def unprocessed
    respond_to do |format|
      format.html
      format.json {
        render json: Robot.unprocessed(params)
      }
    end
  end

  def qa_passed
    respond_to do |format|
      format.html
      format.json {
        render json: Robot.qa_passed(params)
      }
    end
  end

  def factory_second
    respond_to do |format|
      format.html
      format.json {
        render json: Robot.factory_second(params)
      }
    end
  end

  def extinguish
    render json: { success: action.extinguish }
  end

  def recycle
    render json: { success: action.recycle }
  end

  def qa_pass
    render json: { success: action.set_to_pass }
  end

  def send_to_second_factory
    render json: { success: action.send_to_second_factory }
  end

  private

  def init_qa_action
    @action ||= Robot::QualityAssurance::Actions.new(params)
  end

end
