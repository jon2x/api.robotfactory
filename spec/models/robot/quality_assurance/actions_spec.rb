require 'spec_helper'

RSpec.describe Robot::QualityAssurance::Actions do
  describe 'actions' do
    let!(:robot) { FactoryGirl.create(:robot, :on_fire) }
    let!(:action) { Robot::QualityAssurance::Actions.new(robot_ids: [robot.id]) }

    context 'extinguish' do
      it 'should created an on fire robot' do
        expect(robot.is_on_fire?).to be_truthy
      end

      it 'should extinguish the fire' do
        action.extinguish
        expect(robot.on_fire_statuses.any?).to be_falsey
        expect(robot.is_on_fire?).to be_falsey
      end
    end

    context 'recycle' do
      it 'should able to recycle' do
        action.recycle
        expect(robot.recycled_statuses.any?).to be_truthy
        expect(robot.has_been_recycled?).to be_truthy
      end
    end

    describe 'passing qa stages' do
      let!(:robot) { FactoryGirl.create(:robot, :will_pass_the_qa) }
      let!(:action) { Robot::QualityAssurance::Actions.new(robot_ids: [robot.id]) }
      
      context 'passing the qa' do
        it 'should pass the qa' do
          action.set_to_pass
          expect(robot.qa_passed_statuses.any?).to be_truthy
          expect(robot.have_passed_from_qa?).to be_truthy
        end
      end

      context 'sending to second factory' do
        it 'should be sent to second factory' do
          action.send_to_second_factory
          expect(robot.second_factory_statuses.any?).to be_truthy
          expect(robot.has_been_sent_to_second_factory?).to be_truthy
        end
      end
    end

    describe 'shipping the robots' do
      let!(:robot) { FactoryGirl.create(:robot, :qa_passed) }
      let!(:action) { Robot::QualityAssurance::Actions.new(robot_ids: [robot.id]) }

      it 'should shipped the robots' do
        action.ship_it
        robot.reload
        expect(robot.shipped_statuses.any?).to be_truthy
        expect(robot.has_been_shipped?).to be_truthy
        expect(robot.shipment.name.present?).to be_truthy
      end

      context 'should not allow bad robot to be shipped' do
        let!(:robot) { FactoryGirl.create(:robot, :on_fire) }
        let!(:action) { Robot::QualityAssurance::Actions.new(robot_ids: [robot.id]) }

        it 'should raise an exception when shipping' do
          expect { action.ship_it }.to raise_error "You can't ship a robot that does not passed the qa"
        end
      end
    end
  end
end