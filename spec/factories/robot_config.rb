FactoryGirl.define do
  factory :robot_config, class: RobotConfig do
    has_sentience Faker::Boolean.boolean
    has_wheels Faker::Boolean.boolean
    has_tracks Faker::Boolean.boolean
    number_of_rotors Faker::Number.between(1, 10)
    colour Faker::Color.color_name

    trait :on_fire do
      has_sentience true
    end

    trait :recycled do
      has_sentience Faker::Boolean.boolean
      has_wheels true
      has_tracks true
      number_of_rotors Faker::Number.between(1, 7)
      colour 'blue'
    end

    trait :qa_passed do
      has_sentience false
      has_wheels false
      has_tracks false
      number_of_rotors 8
      colour 'red'
    end

    trait :will_pass_the_qa do
      has_sentience false
      has_wheels false
      has_tracks false
      number_of_rotors 8
      colour 'red'
    end
  end
end