FactoryGirl.define do
  factory :robot, class: Robot do
    name Faker::Superhero.name

    trait :on_fire do
      after(:create) do |robot|
        FactoryGirl.create(:robot_status, :on_fire, robot: robot)
        FactoryGirl.create(:robot_config, :on_fire, robot: robot)
      end
    end

    trait :recycled do
      after(:create) do |robot|
        FactoryGirl.create(:robot_status, :recycled, robot: robot)
        FactoryGirl.create(:robot_config, :recycled, robot: robot)
      end
    end

    trait :will_pass_the_qa do
      after(:create) do |robot|
        FactoryGirl.create(:robot_status, robot: robot, status: nil)
        FactoryGirl.create(:robot_config, :will_pass_the_qa, robot: robot)
      end
    end

    trait :qa_passed do
      after(:create) do |robot|
        FactoryGirl.create(:robot_status, :qa_passed, robot: robot)
        FactoryGirl.create(:robot_config, :qa_passed, robot: robot)
      end
    end
  end
end