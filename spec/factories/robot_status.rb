FactoryGirl.define do
  factory :robot_status, class: RobotStatus do
    status [
      RobotStatus::ON_FIRE, RobotStatus::RUSTY, RobotStatus::LOOSE_SCREWS, 
      RobotStatus::PAINT_SCRATCHED, RobotStatus::FACTORY_SECOND, 
      RobotStatus::PASSED_QA, RobotStatus::SHIPPED, RobotStatus::RECYCLED
    ].sample

    trait :on_fire do
      status RobotStatus::ON_FIRE
    end

    trait :recycled do
      status [RobotStatus::ON_FIRE, RobotStatus::LOOSE_SCREWS, RobotStatus::RUSTY].sample
    end

    trait :qa_passed do
      status RobotStatus::PASSED_QA
    end
  end
end