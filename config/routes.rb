Rails.application.routes.draw do
  root to: 'dashboard#index'

  resources :robots do
    collection do
      get :unprocessed
      get :qa_passed
      get :factory_second
      post :extinguish
      post :recycle
      post :qa_pass
      post :send_to_second_factory
    end
    member do; end
  end

  resources :shipments
end
